# The Hotels API Controller
module Api::V1
  class HotelsController < ApplicationController
    # Validate the requires params before the index action
    before_action :validate_params, only: [:index]

    # GET /api/v1/hotels
    # @param [String] city
    # @param [Date] fromDate
    # @param [Date] toDate
    # @param [Integer] numberOfAdults
    def index
      # Getting all the hotels after sanitizing the user params
      hotels = Hotel.where(city_code: params[:city])
                    .where('number_of_adults >= ?', params[:numberOfAdults])
                    .where('available_from >= ? AND ? <= available_to',
                           Date.parse(params[:fromDate]).to_s, Date.parse(params[:toDate]).to_s)
                    .select('name as hotel,
                             hotel_rate as hotelRate,
                             hotel_fare as hotelFare,
                             room_amenities as roomAmenities')
      # Render the result as JSON
      render json: hotels.to_json(except: %i[id]).to_s
    end

    protected

    # The params validator method
    # TODO: Validate the date params
    def validate_params
      if params[:city].blank? || \
         params[:fromDate].blank? || \
         params[:toDate].blank? || \
         params[:numberOfAdults].blank?
        render json: { error: 'Please send the required params' }, status: :bad_request
      end
    end
  end
end
