# hotels_controller.rb
class HotelsControllerTest < ActionDispatch::IntegrationTest
  test 'Hotels list without required params should fail' do
    get api_v1_hotels_path
    assert_response 400
  end

  test 'Hotels list with required params should work' do
    get api_v1_hotels_path, params: {city: 'DUBAI', fromDate: '2018-10-17', toDate: '2018-10-19', numberOfAdults: 1}
    assert_response 200
  end
end